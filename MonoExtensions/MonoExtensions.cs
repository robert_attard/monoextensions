﻿using UnityEngine;
using System.Collections;

public enum DrawMethod
{
	WorldView,
	Viewport,
	ScreenView
}


namespace MonoExtensions
{

	public static class ScriptExtensions
	{
		public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius,float torquePush)
		{
			var dir = (body.transform.position - explosionPosition);
			float wearoff = 1 - (Mathf.Min(dir.magnitude,explosionRadius) / explosionRadius);
			body.AddForce(dir.normalized * explosionForce * wearoff);
//			body.AddTorque(Random.Range(-torquePush,torquePush),ForceMode2D.Impulse);
		}

//		public static void DrawCross(Vector3 position, float size, Color color, DrawMethod method, Camera cam)
//		{
//			Debug.DrawLine(new Vector3(position.x-(size/2f),position.y,position.z),new Vector3(position.x+(size/2f),position.y,position.z),color);
//			Debug.DrawLine(new Vector3(position.x,position.y - (size/2f),position.z),new Vector3(position.x,position.y + (size/2f),position.z),color);
//		}


		public static void DrawCircle(Vector3 position, float radius, Color color,Vector3 upVector,DrawMethod drawMethod, Camera cam, int points = 24)
		{
//			position = position.ConvertToDrawMode(drawMethod,cam);

			Quaternion rot = Quaternion.FromToRotation(Vector3.up,upVector);
			Vector3 previousPt =  rot * (new Vector3(position.x + radius * Mathf.Cos (0),position.y + radius * Mathf.Sin (0),position.z)).ConvertToDrawMode(drawMethod,cam);

			float step = 360f/24f;
			for(float i=step;i<=360f;i+= step )
			{
				Vector3 nextPt = new Vector3(position.x + radius * Mathf.Cos (Mathf.Deg2Rad * i),position.y + radius * Mathf.Sin (Mathf.Deg2Rad * i),position.z);
				nextPt = (position + (rot * (nextPt -position))).ConvertToDrawMode(drawMethod,cam);

				Debug.DrawLine(previousPt,nextPt,color);
				previousPt = nextPt;
			}
		}

		public static Vector3 ConvertToDrawMode(this Vector3 point,DrawMethod drawMethod, Camera cam)
		{
			switch(drawMethod)
			{
			case DrawMethod.WorldView:
				break;
			case DrawMethod.Viewport:
				point.z = cam.transform.position.z +100f;
				return cam.ViewportToWorldPoint(point);
				break;
			case DrawMethod.ScreenView:
				return cam.ScreenToWorldPoint(point);
				break;
			}
			return point;
		}
		
//		public static void DrawBox(Vector3 position, float size, Color color, bool drawCross = false)
//		{
//			Debug.DrawLine(new Vector3(position.x-(size/2f),position.y + (size/2f),position.z),new Vector3(position.x+(size/2f),position.y + (size/2f),position.z),color);
//			Debug.DrawLine(new Vector3(position.x-(size/2f),position.y - (size/2f),position.z),new Vector3(position.x+(size/2f),position.y - (size/2f),position.z),color);
//			Debug.DrawLine(new Vector3(position.x-(size/2f),position.y + (size/2f),position.z),new Vector3(position.x-(size/2f),position.y - (size/2f),position.z),color);
//			Debug.DrawLine(new Vector3(position.x+(size/2f),position.y + (size/2f),position.z),new Vector3(position.x+(size/2f),position.y - (size/2f),position.z),color);
//			if(drawCross)
//			{
//				Debug.DrawLine(new Vector3(position.x-(size/2f),position.y + (size/2f),position.z),new Vector3(position.x+(size/2f),position.y - (size/2f),position.z),color);
//				Debug.DrawLine(new Vector3(position.x-(size/2f),position.y - (size/2f),position.z),new Vector3(position.x+(size/2f),position.y + (size/2f),position.z),color);
//			}
//		}
//
//		public static void DrawCross(Vector3 position, float size, Color color)
//		{
//			Debug.DrawLine(new Vector3(position.x-(size/2f),position.y,position.z),new Vector3(position.x+(size/2f),position.y,position.z),color);
//			Debug.DrawLine(new Vector3(position.x,position.y - (size/2f),position.z),new Vector3(position.x,position.y + (size/2f),position.z),color);
//		}

		public static Vector3 ReplaceComponent(this Vector3 v,VectorComponent component, float newValue )
		{
			switch(component)
			{
				case VectorComponent.X:
					return new Vector3(newValue,v.y,v.z);
				break;
				case VectorComponent.Y:
					return new Vector3(v.x,newValue,v.z);
				break;
				case VectorComponent.Z:
					return new Vector3(v.x,v.y,newValue);
				break;
			}
			return v;
		}

		public static float MouseDistanceFromViewportCenter()
		{
			return MouseDistanceFromViewportCenter(Camera.main);
		}

		public static float MouseDistanceFromViewportCenter(Camera camera)
		{
			return Vector2.Distance((Vector2)camera.ScreenToViewportPoint(Input.mousePosition),new Vector2(0.5f,0.5f));
		}

		public static float AngleDir(this Vector3 from , Vector3 to ,Vector3 up ) {
			float result=0;
			Vector3 perp  = Vector3.Cross(from, to);
			float dir  = Vector3.Dot(perp, up);
			
			if (dir > 0.0) {
				result = 1.0f;
			} else if (dir < 0.0) {
				result = -1.0f;
			}
			
			return result * (Vector3.Angle(from, to));
		}
	}
}

public enum VectorComponent
{
	X,
	Y,
	Z
}